<?php 
    $dsn = "mysql:dbname=lojadoandre;host=localhost"; // Nome do banco e Host do banco
    $dbuser = "root"; // Usuario do banco
    $dbpass = ""; // senha do banco

    try {
        $pdo = new PDO($dsn, $dbuser, $dbpass); //aqui a conexão acontece
        
    } catch (PDOExeption $e) {
        echo "ERROR: ".$e->getMessage(); // aqui exibe uma mensagem de erro caso falhe a conexão
        exit;
    }
?>